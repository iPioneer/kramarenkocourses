package task6;

/**
 * End-2-End test and feature tests with helper method to setup preconditions
 */

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.StringJoiner;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static task6.ToDoMVCTest.TaskStatus.ACTIVE;
import static task6.ToDoMVCTest.TaskStatus.COMPLETED;

public class ToDoMVCTest {
    ElementsCollection tasks = $$("#todo-list li");

    @Test
    public void testAddAtAnyTab() {
        given(getTask(ACTIVE, "A"), getTask(COMPLETED, "B"));

        assertTasks("A", "B");
        assertItemsLeft(1);
    }

    @Test
    public void testAddAtActive() {
        givenAtActive(getTask(ACTIVE, "A"), getTask(ACTIVE, "B"));

        assertTasks("A", "B");
        assertItemsLeft(2);
    }

    @Test
    public void testAddAtCompleted() {
        givenAtCompleted(getTask(COMPLETED, "A"), getTask(COMPLETED, "B"));

        assertTasks("A", "B");
        assertItemsLeft(0);
    }

    @Test
    public void testAddAtActiveSecondMethod() {
        given(ACTIVE, "A", "B");

        filterActive();
        assertTasks("A", "B");
        assertItemsLeft(2);
    }

    @Test
    public void testAddAtActiveThirdMethod() {
        givenAtActive(ACTIVE, "A", "B");

        assertTasks("A", "B");
        assertItemsLeft(2);
    }


    @Test
    public void testAddAtCompletedSecondMethod() {
        given(COMPLETED, "A", "B");

        filterCompleted();
        assertTasks("A", "B");
        assertItemsLeft(0);
    }


    @Test
    public void testCancelEdit() {
        givenAtActive(getTask(ACTIVE, "B"), getTask(ACTIVE, "C"));

        cancelEdit("B", "B Edited");
        assertTasks("B", "C");
        assertItemsLeft(2);
    }

    @Test
    public void testTasksCommonFlow() {
        given();
        add("A");
        //complete
        toggle("A");
        assertTasks("A");

        filterActive();
        assertNoVisibleTasks();
        add("B");
        assertItemsLeft(1);
        assertVisibleTasks("B");
        //complete all
        toggleAll();
        assertNoVisibleTasks();

        filterCompleted();
        assertVisibleTasks("A", "B");
        //reopen
        toggle("B");
        assertVisibleTasks("A");
        clearCompleted();
        assertNoVisibleTasks();

        filterAll();
        delete("B");
        assertNoTasks();
    }

    private void given(Task... tasks) {

        ensureMainUrl();

        StringJoiner joiner = new StringJoiner(", ");

        for (Task task : tasks) {
            joiner.add("{\\\"completed\\\":" + task.taskStatus + ", \\\"title\\\":\\\"" + task.taskText + "\\\"}");
        }

        String result = "localStorage.setItem(\"todos-troopjs\", \"[" + joiner + "]\")";

        executeJavaScript(result);
        executeJavaScript("location.reload()");
    }

    private void givenAtActive(Task... tasks) {
        filterActive();
        given(tasks);
    }

    private void givenAtCompleted(Task... tasks) {
        filterCompleted();
        given(tasks);
    }


    private void given(TaskStatus taskStatus, String... taskTexts) {

        given(getTasks(taskStatus, taskTexts));

    }


    private void givenAtActive(TaskStatus taskStatus, String... taskTexts) {
        filterActive();
        given(getTasks(taskStatus, taskTexts));

    }

    private void givenAtCompleted(TaskStatus taskStatus, String... taskTexts) {
        filterCompleted();
        given(getTasks(taskStatus, taskTexts));

    }

    public void ensureMainUrl() {
        if (url() != "https://todomvc4tasj.herokuapp.com/") {
            open("https://todomvc4tasj.herokuapp.com/");
        }
    }

    private Task[] getTasks(TaskStatus taskStatus, String... taskTexts) {
        Task[] array = new Task[taskTexts.length];
        for (int i = 0; i < taskTexts.length; i++) {
            array[i] = new Task(taskStatus, taskTexts[i]);
        }
        return array;
    }

    private Task getTask(TaskStatus taskStatus, String taskText) {
        return new Task(taskStatus, taskText);
    }

    private void add(String... taskTexts) {
        for (String text : taskTexts) {
            $("#new-todo").setValue(text).pressEnter();
        }
    }

    private void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().find(".destroy").click();
    }

    private void edit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEnter();
    }

    private void cancelEdit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEscape();
    }

    private SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).find(".edit").setValue(newTaskText);
    }

    private void toggle(String taskText) {
        tasks.find(exactText(taskText)).find(".toggle").click();
    }

    private void toggleAll() {
        $("#toggle-all").click();
    }

    private void filterActive() {
        $(By.linkText("Active")).click();
    }

    private void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    private void filterAll() {
        $(By.linkText("All")).click();
    }

    private void assertItemsLeft(int counter) {
        $("#todo-count>strong").shouldHave(exactText(String.valueOf(counter)));
    }

    private void clearCompleted() {
        $("#clear-completed").click();
    }

    private void assertTasks(String... taskTexts) {
        tasks.shouldHave(exactTexts(taskTexts));
    }

    private void assertVisibleTasks(String... taskTexts) {
        tasks.filter(visible).shouldHave((exactTexts(taskTexts)));
    }

    private void assertNoVisibleTasks() {
        tasks.filter(visible).shouldBe(empty);
    }

    private void assertNoTasks() {
        tasks.shouldBe(empty);
    }

    enum TaskStatus {
        ACTIVE("false"), COMPLETED("true");
        String description;

        TaskStatus(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return description;
        }
    }

    private class Task {
        TaskStatus taskStatus;
        String taskText;

        public Task(TaskStatus taskStatus, String taskText) {
            this.taskStatus = taskStatus;
            this.taskText = taskText;
        }
    }

}