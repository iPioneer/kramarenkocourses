package task1;

/**
 * Simple test of ToDoMVC application
 */

import com.codeborne.selenide.ElementsCollection;
import org.junit.Test;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.*;


/**
 * 1 create task1
 * 2 create task2
 * 3 create task3
 * 4 create task4
 * 5 delete task2
 * 6 mark task4 as completed
 * 7 clear completed
 * 8 mark all as completed
 * 9 clear completed
 */

public class ToDoMVCTest {

    ElementsCollection tasks = $$("#todo-list li");

    @Test
    public void testTasksCommonFlow() {

        open("https://todomvc4tasj.herokuapp.com/");

        add("1", "2", "3", "4");
        assertTasksAre("1", "2", "3", "4");

        delete("2");
        assertTasksAre("1", "3", "4");

        toggle("4");
        clearCompleted();
        assertTasksAre("1", "3");

        toggleAll();
        clearCompleted();
        assertTasksAreEmpty();

    }

    private void add(String... taskTexts) {
        for (String text : taskTexts) {
            $("#new-todo").setValue(text).pressEnter();
        }
    }

    private void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().find(".destroy").click();
    }

    private void toggle(String taskText) {
        tasks.find(exactText(taskText)).find(".toggle").click();
    }

    private void toggleAll() {
        $("#toggle-all").click();
    }

    private void clearCompleted() {
        $("#clear-completed").click();
    }

    private void assertTasksAre(String... tasksTexts) {
        tasks.shouldHave(exactTexts(tasksTexts));
    }

    private void assertTasksAreEmpty() {
        tasks.shouldBe(empty);
    }


}

