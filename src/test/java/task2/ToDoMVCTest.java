package task2;

/**
 * Full end-2-end test
 */

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class ToDoMVCTest {

    ElementsCollection tasks = $$("#todo-list li");

    @Test
    public void testTasksCommonFlow() {
        open("https://todomvc4tasj.herokuapp.com/");

        add("A");
        edit("A", "A Edited");
        //complete
        toggle("A Edited");
        assertTasks("A Edited");

        filterActive();
        assertNoVisibleTasks();
        add("B");
        cancelEdit("B", "B Edited");
        assertItemsLeft(1);
        assertVisibleTasks("B");
        //complete all
        toggleAll();
        assertNoVisibleTasks();

        filterCompleted();
        assertVisibleTasks("A Edited", "B");
        //reopen
        toggle("B");
        assertVisibleTasks("A Edited");
        clearCompleted();
        assertNoVisibleTasks();

        filterAll();
        delete("B");
        assertNoTasks();
    }

    private void add(String... taskTexts) {
        for (String text : taskTexts) {
            $("#new-todo").setValue(text).pressEnter();
        }
    }

    private void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().find(".destroy").click();
    }

    private void edit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEnter();
    }

    private void cancelEdit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEscape();
    }

    private SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).find(".edit").setValue(newTaskText);
    }

    private void toggle(String taskText) {
        tasks.find(exactText(taskText)).find(".toggle").click();
    }

    private void toggleAll() {
        $("#toggle-all").click();
    }

    private void filterActive() {
        $(By.linkText("Active")).click();
    }

    private void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    private void filterAll() {
        $(By.linkText("All")).click();
    }

    private void assertItemsLeft(int counter) {
        $("#todo-count>strong").shouldHave(exactText(String.valueOf(counter)));
    }

    private void clearCompleted() {
        $("#clear-completed").click();
    }

    private void assertTasks(String... taskTexts) {
        tasks.shouldHave(exactTexts(taskTexts));
    }

    private void assertVisibleTasks(String... taskTexts) {
        tasks.filter(visible).shouldHave((exactTexts(taskTexts)));
    }

    private void assertNoVisibleTasks() {
        tasks.filter(visible).shouldBe(empty);
    }

    private void assertNoTasks() {
        tasks.shouldBe(empty);
    }
}
