package task5;

import org.junit.After;
import org.junit.Before;

import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.Selenide.open;

/**
 * End-2-End test and feature tests + Allure reports + Devided between OOP classes logic
 */
public class AtToDoMVCPageWithClearedDataAfterEachTest extends BaseTest {

    @Before
    public void openPage() {
        open("https://todomvc4tasj.herokuapp.com/");
    }

    @After
    public void clearData() {
        executeJavaScript("localStorage.clear()");
    }
}
