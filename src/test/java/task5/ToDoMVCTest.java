package task5;

/**
 * End-2-End test and feature tests + Allure reports + Devided between OOP classes logic
 */

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.Test;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ToDoMVCTest extends AtToDoMVCPageWithClearedDataAfterEachTest{
    ElementsCollection tasks = $$("#todo-list li");





    @Test
    public void testEditTask() {
        add("A");
        edit("A", "A Edited");
        assertTasks("A Edited");
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEdit() {
        add("B");
        cancelEdit("B", "B Edited");
        assertTasks("B");
        assertItemsLeft(1);
    }

    @Test
    public void testTasksCommonFlow() {

        add("A");
        //complete
        toggle("A");
        assertTasks("A");

        filterActive();
        assertNoVisibleTasks();
        add("B");
        assertItemsLeft(1);
        assertVisibleTasks("B");
        //complete all
        toggleAll();
        assertNoVisibleTasks();

        filterCompleted();
        assertVisibleTasks("A", "B");
        //reopen
        toggle("B");
        assertVisibleTasks("A");
        clearCompleted();
        assertNoVisibleTasks();

        filterAll();
        delete("B");
        assertNoTasks();
    }

    @Step
    private void add(String... taskTexts) {
        for (String text : taskTexts) {
            $("#new-todo").setValue(text).pressEnter();
        }
    }

    @Step
    private void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().find(".destroy").click();
    }

    @Step
    private void edit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEnter();
    }

    @Step
    private void cancelEdit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEscape();
    }

    @Step
    private SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).find(".edit").setValue(newTaskText);
    }

    @Step
    private void toggle(String taskText) {
        tasks.find(exactText(taskText)).find(".toggle").click();
    }

    @Step
    private void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    private void filterActive() {
        $(By.linkText("Active")).click();
    }

    @Step
    private void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    @Step
    private void filterAll() {
        $(By.linkText("All")).click();
    }

    @Step
    private void assertItemsLeft(int counter) {
        $("#todo-count>strong").shouldHave(exactText(String.valueOf(counter)));
    }

    @Step
    private void clearCompleted() {
        $("#clear-completed").click();
    }

    @Step
    private void assertTasks(String... taskTexts) {
        tasks.shouldHave(exactTexts(taskTexts));
    }

    @Step
    private void assertVisibleTasks(String... taskTexts) {
        tasks.filter(visible).shouldHave((exactTexts(taskTexts)));
    }

    @Step
    private void assertNoVisibleTasks() {
        tasks.filter(visible).shouldBe(empty);
    }

    @Step
    private void assertNoTasks() {
        tasks.shouldBe(empty);
    }
}