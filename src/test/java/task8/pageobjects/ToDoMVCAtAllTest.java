package task8.pageobjects;

import org.junit.Test;
import task8.pageobjects.pages.ToDoMVCPage;

import static task8.pageobjects.pages.ToDoMVCPage.TaskStatus.ACTIVE;
import static task8.pageobjects.pages.ToDoMVCPage.TaskStatus.COMPLETED;


public class ToDoMVCAtAllTest {

    ToDoMVCPage page = new ToDoMVCPage();

    @Test
    public void testEditOnAll() {
        page.given(page.aTask(ACTIVE, "A"), page.aTask(COMPLETED, "B"));

        page.edit("B", "B Edited");
        page.assertTasks("A", "B Edited");
        page.assertItemsLeft(1);
    }


    @Test
    public void testActivateOnAll() {
        page.given(COMPLETED, "F", "G");

        page.toggle("F");
        page.assertTasks("F", "G");
        page.assertItemsLeft(1);

    }


    @Test
    public void testCompleteAllOnAll() {
        page.given(ACTIVE, "J", "K", "L");

        page.toggleAll();
        page.assertVisibleTasks("J", "K", "L");
        page.assertItemsLeft(0);
    }


    @Test
    public void testClearCompletedOnAll() {
        page.given(page.aTask(ACTIVE, "O"), page.aTask(COMPLETED, "P"));

        page.clearCompleted();
        page.assertItemsLeft(1);
    }

    @Test
    public void testSwitchFromActiveToAll() {
        page.givenOnActive(page.aTask(ACTIVE, "C"), page.aTask(COMPLETED, "D"));

        page.filterAll();
        page.assertTasks("C", "D");
        page.assertItemsLeft(1);
    }

    @Test
    public void testCancelEditOnAll() {
        page.given(page.aTask(ACTIVE, "I"), page.aTask(COMPLETED, "Task J"));

        page.cancelEdit("Task J", "J");
        page.assertTasks("I", "Task J");
        page.assertItemsLeft(1);
    }


    @Test
    public void testConfirmEditByPressTabOnAll() {
        page.given(page.aTask(ACTIVE, "U"), page.aTask(COMPLETED, "Task V"));

        page.confirmEditByTab("Task V", "V");
        page.assertTasks("U", "V");
    }


}
