package task8.pageobjects.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.util.StringJoiner;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;

public class ToDoMVCPage {

    public ElementsCollection tasks = $$("#todo-list li");

    public SelenideElement newTodo = $("#new-todo");

    public void confirmEditByClickOutside(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText);
        newTodo.click();
    }

    public void given(Task... tasks) {

        ensureMainUrl();

        StringJoiner joiner = new StringJoiner(", ");

        for (Task task : tasks) {
            joiner.add("{\\\"completed\\\":" + task.taskStatus + ", \\\"title\\\":\\\"" + task.taskText + "\\\"}");
        }

        String result = "localStorage.setItem(\"todos-troopjs\", \"[" + joiner + "]\")";

        executeJavaScript(result);
        executeJavaScript("location.reload()");
    }

    public void givenOnActive(Task... tasks) {
        given(tasks);
        filterActive();
    }

    public void givenOnCompleted(Task... tasks) {
        given(tasks);
        filterCompleted();
    }

    public void given(TaskStatus taskStatus, String... taskTexts) {

        given(getTasks(taskStatus, taskTexts));

    }

    public void givenOnActive(TaskStatus taskStatus, String... taskTexts) {
        given(getTasks(taskStatus, taskTexts));
        filterActive();

    }

    public void givenOnCompleted(TaskStatus taskStatus, String... taskTexts) {
        given(getTasks(taskStatus, taskTexts));
        filterCompleted();

    }

    public void ensureMainUrl() {
        if (!url().equals("https://todomvc4tasj.herokuapp.com/")) {
            open("https://todomvc4tasj.herokuapp.com/");
        }
    }

    public Task[] getTasks(TaskStatus taskStatus, String... taskTexts) {
        Task[] array = new Task[taskTexts.length];
        for (int i = 0; i < taskTexts.length; i++) {
            array[i] = new Task(taskStatus, taskTexts[i]);
        }
        return array;
    }

    public Task aTask(TaskStatus taskStatus, String taskText) {
        return new Task(taskStatus, taskText);
    }

    public void add(String... taskTexts) {
        for (String text : taskTexts) {
            $("#new-todo").setValue(text).pressEnter();
        }
    }

    public void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().find(".destroy").click();
    }

    public void edit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEnter();
    }

    public void cancelEdit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEscape();
    }

    public void confirmEditByTab(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressTab();
    }

    public SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).find(".edit").setValue(newTaskText);
    }

    public void toggle(String taskText) {
        tasks.find(exactText(taskText)).find(".toggle").click();
    }

    public void toggleAll() {
        $("#toggle-all").click();
    }

    public void filterActive() {
        $(By.linkText("Active")).click();
    }

    public void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    public void filterAll() {
        $(By.linkText("All")).click();
    }

    public void assertItemsLeft(int counter) {
        $("#todo-count>strong").shouldHave(exactText(String.valueOf(counter)));
    }

    public void clearCompleted() {
        $("#clear-completed").click();
    }

    public void assertTasks(String... taskTexts) {
        tasks.shouldHave(exactTexts(taskTexts));
    }

    public void assertVisibleTasks(String... taskTexts) {
        tasks.filter(visible).shouldHave((exactTexts(taskTexts)));
    }

    public void assertNoVisibleTasks() {
        tasks.filter(visible).shouldBe(empty);
    }

    public void assertNoTasks() {
        tasks.shouldBe(empty);
    }

    public enum TaskStatus {
        ACTIVE("false"), COMPLETED("true");
        String description;

        TaskStatus(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return description;
        }
    }

    public class Task {
        TaskStatus taskStatus;
        String taskText;

        public Task(TaskStatus taskStatus, String taskText) {
            this.taskStatus = taskStatus;
            this.taskText = taskText;
        }
    }
}
