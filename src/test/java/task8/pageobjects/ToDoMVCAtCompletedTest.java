package task8.pageobjects;


import org.junit.Test;
import task8.pageobjects.pages.ToDoMVCPage;

import static task8.pageobjects.pages.ToDoMVCPage.TaskStatus.ACTIVE;
import static task8.pageobjects.pages.ToDoMVCPage.TaskStatus.COMPLETED;

public class ToDoMVCAtCompletedTest {

    ToDoMVCPage page = new ToDoMVCPage();

    @Test
    public void testActivateAllOnComplete() {
        page.givenOnCompleted(COMPLETED, "M", "N");

        page.toggleAll();
        page.assertNoVisibleTasks();
        page.assertItemsLeft(2);
    }


    @Test
    public void testSwitchFromAllToCompleted() {
        page.given(page.aTask(ACTIVE, "A"), page.aTask(COMPLETED, "B"));

        page.filterCompleted();
        page.assertVisibleTasks("B");
        page.assertItemsLeft(1);
    }


    @Test
    public void testDeleteTaskByEmptyingTextOnCompleted() {
        page.givenOnCompleted(page.aTask(ACTIVE, "G"), page.aTask(COMPLETED, "Task H"));

        page.edit("Task H", "");
        page.assertNoVisibleTasks();
        page.assertItemsLeft(1);
    }


    @Test
    public void testCancelEditOnCompleted() {
        page.givenOnCompleted(COMPLETED, "S", "Task T");

        page.cancelEdit("Task T", "T");
        page.assertVisibleTasks("S", "Task T");
    }
}
