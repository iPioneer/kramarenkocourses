package task8.pageobjects;

import org.junit.Test;
import task8.pageobjects.pages.ToDoMVCPage;

public class ToDoMVCGeneralTest {

    ToDoMVCPage page = new ToDoMVCPage();

    @Test
    public void testTasksCommonFlow() {
        page.given();
        page.add("A");
        //complete
        page.toggle("A");
        page.assertTasks("A");

        page.filterActive();
        page.assertNoVisibleTasks();
        page.add("B");
        page.assertItemsLeft(1);
        page.assertVisibleTasks("B");
        //complete all
        page.toggleAll();
        page.assertNoVisibleTasks();

        page.filterCompleted();
        page.assertVisibleTasks("A", "B");
        //reopen
        page.toggle("B");
        page.assertVisibleTasks("A");
        page.clearCompleted();
        page.assertNoVisibleTasks();

        page.filterAll();
        page.delete("B");
        page.assertNoTasks();
    }

}