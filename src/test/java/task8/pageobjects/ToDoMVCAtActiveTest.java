package task8.pageobjects;

import org.junit.Test;
import task8.pageobjects.pages.ToDoMVCPage;

import static task8.pageobjects.pages.ToDoMVCPage.TaskStatus.ACTIVE;
import static task8.pageobjects.pages.ToDoMVCPage.TaskStatus.COMPLETED;

public class ToDoMVCAtActiveTest {

    ToDoMVCPage page = new ToDoMVCPage();

    @Test
    public void testEditOnActive() {
        page.given(page.aTask(ACTIVE, "C"), page.aTask(COMPLETED, "D"));

        page.filterActive();
        page.edit("C", "C Edited");
        page.assertVisibleTasks("C Edited");
        page.assertItemsLeft(1);
    }

    @Test
    public void testCompleteOnActive() {
        page.givenOnActive(ACTIVE, "D", "E");

        page.toggle("D");
        page.assertVisibleTasks("E");
        page.assertItemsLeft(1);

    }

    @Test
    public void testDeleteOnActive() {
        page.givenOnActive(ACTIVE, "H", "I");

        page.delete("H");
        page.assertTasks("I");
        page.assertItemsLeft(1);
    }


    @Test
    public void testClearCompletedOnActive() {
        page.givenOnActive(COMPLETED, "R", "S");

        page.clearCompleted();
        page.assertNoTasks();
    }


    @Test
    public void testSwitchFromCompletedToActive() {
        page.givenOnCompleted(page.aTask(ACTIVE, "E"), page.aTask(COMPLETED, "F"));

        page.filterActive();
        page.assertVisibleTasks("E");
        page.assertItemsLeft(1);
    }


    @Test
    public void testCancelEditOnActive() {
        page.givenOnActive(page.aTask(ACTIVE, "Task Q"), page.aTask(COMPLETED, "R"));

        page.cancelEdit("Task Q", "Q");
        page.assertVisibleTasks("Task Q");
    }


    @Test
    public void testConfirmEditByClickOutsideOnActive() {
        page.givenOnActive(ACTIVE, "W", "Task X");

        page.confirmEditByClickOutside("Task X", "X");
        page.assertTasks("W", "X");
    }

}
