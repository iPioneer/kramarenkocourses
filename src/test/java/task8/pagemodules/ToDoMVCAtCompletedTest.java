package task8.pagemodules;

import org.junit.Test;

import static task8.pagemodules.modules.ToDoMVC.*;
import static task8.pagemodules.modules.ToDoMVC.TaskStatus.ACTIVE;
import static task8.pagemodules.modules.ToDoMVC.TaskStatus.COMPLETED;

public class ToDoMVCAtCompletedTest {


    @Test
    public void testActivateAllOnComplete() {
        givenOnCompleted(COMPLETED, "M", "N");

        toggleAll();
        assertNoVisibleTasks();
        assertItemsLeft(2);
    }

    @Test
    public void testSwitchFromAllToCompleted() {
        given(aTask(ACTIVE, "A"), aTask(COMPLETED, "B"));

        filterCompleted();
        assertVisibleTasks("B");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteTaskByEmptyingTextOnCompleted() {
        givenOnCompleted(aTask(ACTIVE, "G"), aTask(COMPLETED, "Task H"));

        edit("Task H", "");
        assertNoVisibleTasks();
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEditOnCompleted() {
        givenOnCompleted(COMPLETED, "S", "Task T");

        cancelEdit("Task T", "T");
        assertVisibleTasks("S", "Task T");
    }


}
