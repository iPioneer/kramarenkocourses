package task8.pagemodules;


import org.junit.Test;

import static task8.pagemodules.modules.ToDoMVC.*;

public class ToDoMVCGeneralTest {

    @Test
    public void testTasksCommonFlow() {
        given();
        add("A");
        //complete
        toggle("A");
        assertTasks("A");

        filterActive();
        assertNoVisibleTasks();
        add("B");
        assertItemsLeft(1);
        assertVisibleTasks("B");
        //complete all
        toggleAll();
        assertNoVisibleTasks();

        filterCompleted();
        assertVisibleTasks("A", "B");
        //reopen
        toggle("B");
        assertVisibleTasks("A");
        clearCompleted();
        assertNoVisibleTasks();

        filterAll();
        delete("B");
        assertNoTasks();
    }


}