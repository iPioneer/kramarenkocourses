package task8.pagemodules;


import org.junit.Test;

import static task8.pagemodules.modules.ToDoMVC.*;
import static task8.pagemodules.modules.ToDoMVC.TaskStatus.ACTIVE;
import static task8.pagemodules.modules.ToDoMVC.TaskStatus.COMPLETED;

public class ToDoMVCAtActiveTest {


    @Test
    public void testEditOnActive() {
        given(aTask(ACTIVE, "C"), aTask(COMPLETED, "D"));

        filterActive();
        edit("C", "C Edited");
        assertVisibleTasks("C Edited");
        assertItemsLeft(1);
    }

    @Test
    public void testCompleteOnActive() {
        givenOnActive(ACTIVE, "D", "E");

        toggle("D");
        assertVisibleTasks("E");
        assertItemsLeft(1);

    }


    @Test
    public void testDeleteOnActive() {
        givenOnActive(ACTIVE, "H", "I");

        delete("H");
        assertTasks("I");
        assertItemsLeft(1);
    }


    @Test
    public void testClearCompletedOnActive() {
        givenOnActive(COMPLETED, "R", "S");

        clearCompleted();
        assertNoTasks();
    }


    @Test
    public void testSwitchFromCompletedToActive() {
        givenOnCompleted(aTask(ACTIVE, "E"), aTask(COMPLETED, "F"));

        filterActive();
        assertVisibleTasks("E");
        assertItemsLeft(1);
    }


    @Test
    public void testCancelEditOnActive() {
        givenOnActive(aTask(ACTIVE, "Task Q"), aTask(COMPLETED, "R"));

        cancelEdit("Task Q", "Q");
        assertVisibleTasks("Task Q");
    }


    @Test
    public void testConfirmEditByClickOutsideOnActive() {
        givenOnActive(ACTIVE, "W", "Task X");

        confirmEditByClickOutside("Task X", "X");
        assertTasks("W", "X");
    }

}
