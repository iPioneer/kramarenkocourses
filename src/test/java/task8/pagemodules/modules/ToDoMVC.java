package task8.pagemodules.modules;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.util.StringJoiner;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;

public class ToDoMVC {

    public static ElementsCollection tasks = $$("#todo-list li");

    public static SelenideElement newTodo = $("#new-todo");

    public static  void confirmEditByClickOutside(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText);
        newTodo.click();
    }

    public static  void given(Task... tasks) {

        ensureMainUrl();

        StringJoiner joiner = new StringJoiner(", ");

        for (Task task : tasks) {
            joiner.add("{\\\"completed\\\":" + task.taskStatus + ", \\\"title\\\":\\\"" + task.taskText + "\\\"}");
        }

        String result = "localStorage.setItem(\"todos-troopjs\", \"[" + joiner + "]\")";

        executeJavaScript(result);
        executeJavaScript("location.reload()");
    }

    public static  void givenOnActive(Task... tasks) {
        given(tasks);
        filterActive();
    }

    public static  void givenOnCompleted(Task... tasks) {
        given(tasks);
        filterCompleted();
    }

    public static  void given(TaskStatus taskStatus, String... taskTexts) {

        given(getTasks(taskStatus, taskTexts));

    }

    public static  void givenOnActive(TaskStatus taskStatus, String... taskTexts) {
        given(getTasks(taskStatus, taskTexts));
        filterActive();

    }

    public static  void givenOnCompleted(TaskStatus taskStatus, String... taskTexts) {
        given(getTasks(taskStatus, taskTexts));
        filterCompleted();

    }

    public static  void ensureMainUrl() {
        if (!url().equals("https://todomvc4tasj.herokuapp.com/")) {
            open("https://todomvc4tasj.herokuapp.com/");
        }
    }

    public static  Task[] getTasks(TaskStatus taskStatus, String... taskTexts) {
        Task[] array = new Task[taskTexts.length];
        for (int i = 0; i < taskTexts.length; i++) {
            array[i] = new Task(taskStatus, taskTexts[i]);
        }
        return array;
    }

    public static  Task aTask(TaskStatus taskStatus, String taskText) {
        return new Task(taskStatus, taskText);
    }

    public static  void add(String... taskTexts) {
        for (String text : taskTexts) {
            $("#new-todo").setValue(text).pressEnter();
        }
    }

    public static  void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().find(".destroy").click();
    }

    public static  void edit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEnter();
    }

    public static  void cancelEdit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEscape();
    }

    public static  void confirmEditByTab(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressTab();
    }

    public static  SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).find(".edit").setValue(newTaskText);
    }

    public static  void toggle(String taskText) {
        tasks.find(exactText(taskText)).find(".toggle").click();
    }

    public static  void toggleAll() {
        $("#toggle-all").click();
    }

    public static  void filterActive() {
        $(By.linkText("Active")).click();
    }

    public static  void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    public static  void filterAll() {
        $(By.linkText("All")).click();
    }

    public static  void assertItemsLeft(int counter) {
        $("#todo-count>strong").shouldHave(exactText(String.valueOf(counter)));
    }

    public static  void clearCompleted() {
        $("#clear-completed").click();
    }

    public static  void assertTasks(String... taskTexts) {
        tasks.shouldHave(exactTexts(taskTexts));
    }

    public static  void assertVisibleTasks(String... taskTexts) {
        tasks.filter(visible).shouldHave((exactTexts(taskTexts)));
    }

    public static  void assertNoVisibleTasks() {
        tasks.filter(visible).shouldBe(empty);
    }

    public static  void assertNoTasks() {
        tasks.shouldBe(empty);
    }

    public enum TaskStatus {
        ACTIVE("false"), COMPLETED("true");
        String description;

        TaskStatus(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return description;
        }
    }

    public static class Task {
        TaskStatus taskStatus;
        String taskText;

        public Task(TaskStatus taskStatus, String taskText) {
            this.taskStatus = taskStatus;
            this.taskText = taskText;
        }
    }
}
