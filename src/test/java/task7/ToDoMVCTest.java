package task7;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.StringJoiner;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static task7.ToDoMVCTest.TaskStatus.ACTIVE;
import static task7.ToDoMVCTest.TaskStatus.COMPLETED;

public class ToDoMVCTest {

    ElementsCollection tasks = $$("#todo-list li");

    @Test
    public void testTasksCommonFlow() {
        given();
        add("A");
        //complete
        toggle("A");
        assertTasks("A");

        filterActive();
        assertNoVisibleTasks();
        add("B");
        assertItemsLeft(1);
        assertVisibleTasks("B");
        //complete all
        toggleAll();
        assertNoVisibleTasks();

        filterCompleted();
        assertVisibleTasks("A", "B");
        //reopen
        toggle("B");
        assertVisibleTasks("A");
        clearCompleted();
        assertNoVisibleTasks();

        filterAll();
        delete("B");
        assertNoTasks();
    }

    @Test
    public void testEditOnAll() {
        given(aTask(ACTIVE, "A"), aTask(COMPLETED, "B"));

        edit("B", "B Edited");
        assertTasks("A", "B Edited");
        assertItemsLeft(1);
    }

    @Test
    public void testEditOnActive() {
        given(aTask(ACTIVE, "C"), aTask(COMPLETED, "D"));

        filterActive();
        edit("C", "C Edited");
        assertVisibleTasks("C Edited");
        assertItemsLeft(1);
    }

    @Test
    public void testCompleteOnActive() {
        givenOnActive(ACTIVE, "D", "E");

        toggle("D");
        assertVisibleTasks("E");
        assertItemsLeft(1);

    }

    @Test
    public void testActivateOnAll() {
        given(COMPLETED, "F", "G");

        toggle("F");
        assertTasks("F", "G");
        assertItemsLeft(1);

    }

    @Test
    public void testDeleteOnActive() {
        givenOnActive(ACTIVE, "H", "I");

        delete("H");
        assertTasks("I");
        assertItemsLeft(1);
    }

    @Test
    public void testCompleteAllOnAll() {
        given(ACTIVE, "J", "K", "L");

        toggleAll();
        assertVisibleTasks("J", "K", "L");
        assertItemsLeft(0);
    }

    @Test
    public void testActivateAllOnComplete() {
        givenOnCompleted(COMPLETED, "M", "N");

        toggleAll();
        assertNoVisibleTasks();
        assertItemsLeft(2);
    }

    @Test
    public void testClearCompletedOnAll() {
        given(aTask(ACTIVE, "O"), aTask(COMPLETED, "P"));

        clearCompleted();
        assertItemsLeft(1);
    }

    @Test
    public void testClearCompletedOnActive() {
        givenOnActive(COMPLETED, "R", "S");

        clearCompleted();
        assertNoTasks();
    }

    @Test
    public void testSwitchFromAllToCompleted() {
        given(aTask(ACTIVE, "A"), aTask(COMPLETED, "B"));

        filterCompleted();
        assertVisibleTasks("B");
        assertItemsLeft(1);
    }

    @Test
    public void testSwitchFromActiveToAll() {
        givenOnActive(aTask(ACTIVE, "C"), aTask(COMPLETED, "D"));

        filterAll();
        assertTasks("C", "D");
        assertItemsLeft(1);
    }

    @Test
    public void testSwitchFromCompletedToActive() {
        givenOnCompleted(aTask(ACTIVE, "E"), aTask(COMPLETED, "F"));

        filterActive();
        assertVisibleTasks("E");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteTaskByEmptyingTextOnCompleted() {
        givenOnCompleted(aTask(ACTIVE, "G"), aTask(COMPLETED, "Task H"));

        edit("Task H", "");
        assertNoVisibleTasks();
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEditOnAll() {
        given(aTask(ACTIVE, "I"), aTask(COMPLETED, "Task J"));

        cancelEdit("Task J", "J");
        assertTasks("I", "Task J");
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEditOnActive() {
        givenOnActive(aTask(ACTIVE, "Task Q"), aTask(COMPLETED, "R"));

        cancelEdit("Task Q", "Q");
        assertVisibleTasks("Task Q");
    }

    @Test
    public void testCancelEditOnCompleted() {
        givenOnCompleted(COMPLETED, "S", "Task T");

        cancelEdit("Task T", "T");
        assertVisibleTasks("S", "Task T");
    }

    @Test
    public void testConfirmEditByPressTabOnAll() {
        given(aTask(ACTIVE, "U"), aTask(COMPLETED, "Task V"));

        confirmEditByTab("Task V", "V");
        assertTasks("U", "V");
    }

    @Test
    public void testConfirmEditByClickOutsideOnActive() {
        givenOnActive(ACTIVE, "W", "Task X");

        confirmEditByClickOutside("Task X", "X");
        assertTasks("W", "X");
    }

    SelenideElement newTodo = $("#new-todo");

    private void confirmEditByClickOutside(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText);
        newTodo.click();
    }

    private void given(Task... tasks) {

        ensureMainUrl();

        StringJoiner joiner = new StringJoiner(", ");

        for (Task task : tasks) {
            joiner.add("{\\\"completed\\\":" + task.taskStatus + ", \\\"title\\\":\\\"" + task.taskText + "\\\"}");
        }

        String result = "localStorage.setItem(\"todos-troopjs\", \"[" + joiner + "]\")";

        executeJavaScript(result);
        executeJavaScript("location.reload()");
    }

    private void givenOnActive(Task... tasks) {
        given(tasks);
        filterActive();
    }

    private void givenOnCompleted(Task... tasks) {
        given(tasks);
        filterCompleted();
    }

    private void given(TaskStatus taskStatus, String... taskTexts) {

        given(getTasks(taskStatus, taskTexts));

    }

    private void givenOnActive(TaskStatus taskStatus, String... taskTexts) {
        given(getTasks(taskStatus, taskTexts));
        filterActive();

    }

    private void givenOnCompleted(TaskStatus taskStatus, String... taskTexts) {
        given(getTasks(taskStatus, taskTexts));
        filterCompleted();

    }

    public void ensureMainUrl() {
        if (!url().equals("https://todomvc4tasj.herokuapp.com/")) {
            open("https://todomvc4tasj.herokuapp.com/");
        }
    }

    private Task[] getTasks(TaskStatus taskStatus, String... taskTexts) {
        Task[] array = new Task[taskTexts.length];
        for (int i = 0; i < taskTexts.length; i++) {
            array[i] = new Task(taskStatus, taskTexts[i]);
        }
        return array;
    }

    private Task aTask(TaskStatus taskStatus, String taskText) {
        return new Task(taskStatus, taskText);
    }

    private void add(String... taskTexts) {
        for (String text : taskTexts) {
            $("#new-todo").setValue(text).pressEnter();
        }
    }

    private void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().find(".destroy").click();
    }

    private void edit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEnter();
    }

    private void cancelEdit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEscape();
    }

    private void confirmEditByTab(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressTab();
    }

    private SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).find(".edit").setValue(newTaskText);
    }

    private void toggle(String taskText) {
        tasks.find(exactText(taskText)).find(".toggle").click();
    }

    private void toggleAll() {
        $("#toggle-all").click();
    }

    private void filterActive() {
        $(By.linkText("Active")).click();
    }

    private void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    private void filterAll() {
        $(By.linkText("All")).click();
    }

    private void assertItemsLeft(int counter) {
        $("#todo-count>strong").shouldHave(exactText(String.valueOf(counter)));
    }

    private void clearCompleted() {
        $("#clear-completed").click();
    }

    private void assertTasks(String... taskTexts) {
        tasks.shouldHave(exactTexts(taskTexts));
    }

    private void assertVisibleTasks(String... taskTexts) {
        tasks.filter(visible).shouldHave((exactTexts(taskTexts)));
    }

    private void assertNoVisibleTasks() {
        tasks.filter(visible).shouldBe(empty);
    }

    private void assertNoTasks() {
        tasks.shouldBe(empty);
    }

    enum TaskStatus {
        ACTIVE("false"), COMPLETED("true");
        String description;

        TaskStatus(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return description;
        }
    }

    private class Task {
        TaskStatus taskStatus;
        String taskText;

        public Task(TaskStatus taskStatus, String taskText) {
            this.taskStatus = taskStatus;
            this.taskText = taskText;
        }
    }

}