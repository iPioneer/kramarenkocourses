package task9.features;


import org.junit.Test;
import org.junit.experimental.categories.Category;
import task9.categories.Buggy;
import task9.categories.Smoke;

import static task8.pagemodules.modules.ToDoMVC.TaskStatus.ACTIVE;
import static task8.pagemodules.modules.ToDoMVC.TaskStatus.COMPLETED;
import static task8.pagemodules.modules.ToDoMVC.*;

@Category(Smoke.class)
public class ToDoMVCAtAllTest extends BaseTest {

    @Test
    public void testEditOnAll() {
        given(aTask(ACTIVE, "A"), aTask(COMPLETED, "B"));

        edit("B", "B Edited");
        assertTasks("A", "B Edited");
        assertItemsLeft(1);
    }


    @Test
    public void testActivateOnAll() {
        given(COMPLETED, "F", "G");

        toggle("F");
        assertTasks("F", "G");
        assertItemsLeft(1);

    }

    @Test
    public void testCompleteAllOnAll() {
        given(ACTIVE, "J", "K", "L");

        toggleAll();
        assertVisibleTasks("J", "K", "L");
        assertItemsLeft(0);
    }

    @Test
    public void testClearCompletedOnAll() {
        given(aTask(ACTIVE, "O"), aTask(COMPLETED, "P"));

        clearCompleted();
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEditOnAll() {
        given(aTask(ACTIVE, "I"), aTask(COMPLETED, "Task J"));

        cancelEdit("Task J", "J");
        assertTasks("I", "Task J");
        assertItemsLeft(1);
    }

    @Test
    public void testConfirmEditByPressTabOnAll() {
        given(aTask(ACTIVE, "U"), aTask(COMPLETED, "Task V"));

        confirmEditByTab("Task V", "V");
        assertTasks("U", "V");
    }


    @Test
    @Category(Buggy.class)
    public void testSwitchFromActiveToAll() {
        givenOnActive(aTask(ACTIVE, "C"), aTask(COMPLETED, "D"));

        filterAll();
        assertTasks("C", "D");
        assertItemsLeft(2);
    }

}
