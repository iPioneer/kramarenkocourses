package task9.features;


import org.junit.Test;
import org.junit.experimental.categories.Category;
import task9.categories.Smoke;

import static task8.pagemodules.modules.ToDoMVC.*;



@Category(Smoke.class)
public class ToDoMVCGeneralTest extends BaseTest {

    @Test
    public void testTasksCommonFlow() {
        given();
        add("A");
        //complete
        toggle("A");
        assertTasks("A");

        filterActive();
        assertNoVisibleTasks();
        add("B");
        assertItemsLeft(1);
        assertVisibleTasks("B");
        //complete all
        toggleAll();
        assertNoVisibleTasks();

        filterCompleted();
        assertVisibleTasks("A", "B");
        //reopen
        toggle("B");
        assertVisibleTasks("A");
        clearCompleted();
        assertNoVisibleTasks();

        filterAll();
        delete("B");
        assertNoTasks();
    }


}