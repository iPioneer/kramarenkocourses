package task9;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import task9.categories.Buggy;
import task9.categories.Smoke;
import task9.features.ToDoMVCAtAllTest;
import task9.features.ToDoMVCGeneralTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(Smoke.class)
@Suite.SuiteClasses({ToDoMVCAtAllTest.class,ToDoMVCGeneralTest.class})
@Categories.ExcludeCategory(Buggy.class)

public class FullAcceptanceSuiteTest {
}
