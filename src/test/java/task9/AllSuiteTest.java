package task9;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import task9.features.ToDoMVCAtAllTest;
import task9.features.ToDoMVCGeneralTest;

@RunWith(Categories.class)
@Suite.SuiteClasses({ToDoMVCGeneralTest.class, ToDoMVCAtAllTest.class})

public class AllSuiteTest {
}
